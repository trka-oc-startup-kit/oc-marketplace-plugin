<?php return [
    'plugin' => [
        'name' => 'Marketplace',
        'description' => 'Support User-Contrib downloads marketplace. Eg: Plugins, Themes, similar packages.',
    ],
    'download' => [
        'name' => 'Mixin Name',
        'description' => 'Mixin Description',
        'slug' => 'Slug',
        'tags' => 'Tags',
        'packagefile' => 'Package File',
        'type' => 'Download Type',
        'tabs' => [
            'file' => 'File',
            'details' => 'Details',
        ],
        'fields' => [
            'user' => 'User',
        ],
    ],
    'downloadtype' => [
        'label' => 'Label',
        'slug' => 'Slug',
    ],
    'menulabels' => [
        'downloads' => 'Marketplace',
        'downloadtypes' => 'Download Types',
        'downloadtags' => 'Tags',
    ],
    'downloadtags' => [
        'label' => 'Label',
        'slug' => 'Slug',
    ],
];